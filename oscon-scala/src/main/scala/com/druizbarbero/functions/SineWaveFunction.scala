package com.druizbarbero.functions

import java.util
import java.util.Collections
import java.util.concurrent.atomic.AtomicInteger

import scala.collection.JavaConversions._
import com.druizbarbero.data.DataPoint
import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.configuration.Configuration
import org.apache.flink.metrics.Counter
import org.apache.flink.streaming.api.checkpoint.ListCheckpointed

/*
 * Expects a sawtooth wave as input.
 */
class SineWaveFunction extends RichMapFunction[DataPoint[Double], DataPoint[Double]] with ListCheckpointed[Integer] {

    // State
    private var datapoints: Counter = _
    private var currentStep = 0

    override def open(config: Configuration): Unit = {
        this.datapoints = getRuntimeContext.getMetricGroup.counter("datapoints")
    }

    override def map(dataPoint: DataPoint[Double]): DataPoint[Double] = {
        val phase = dataPoint.value.get * 2 * Math.PI
        datapoints.inc()
        dataPoint.withNewValue(Some(Math.sin(phase)))
    }

    override def restoreState(state: util.List[Integer]): Unit = {
        for (s <- state)
            this.currentStep = s
    }

    @throws[Exception]
    override def snapshotState(checkpointId: Long, checkpointTimestamp: Long): util.List[Integer] = Collections.singletonList(currentStep)
}
