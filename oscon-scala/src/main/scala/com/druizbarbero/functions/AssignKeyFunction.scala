package com.druizbarbero.functions

import com.druizbarbero.data.{DataPoint, KeyedDataPoint}
import org.apache.flink.api.common.functions.MapFunction


class AssignKeyFunction(var key: String) extends MapFunction[DataPoint[Double], KeyedDataPoint[Double]] {

    override def map(dataPoint: DataPoint[Double]): KeyedDataPoint[Double] = dataPoint.withKey(key)
}
