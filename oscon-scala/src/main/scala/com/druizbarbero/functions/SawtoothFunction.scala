package com.druizbarbero.functions

import com.druizbarbero.data.DataPoint
import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.checkpoint.ListCheckpointed
import org.apache.flink.metrics.Counter

import scala.collection.JavaConversions._
import java.util.Collections
import java.util
import java.util.concurrent.atomic.AtomicInteger

class SawtoothFunction(val numSteps: Int) extends RichMapFunction[DataPoint[Long], DataPoint[Double]] with ListCheckpointed[Integer] {

    // State
    private var datapoints: Counter = _
    private var currentStep = 0

    override def open(config: Configuration): Unit = {
        datapoints = getRuntimeContext.getMetricGroup.counter("datapoints")
    }

    override def map(dataPoint: DataPoint[Long]): DataPoint[Double] = {
        val phase = currentStep.toDouble / numSteps
        currentStep = {currentStep += 1; currentStep} % numSteps
        datapoints.inc()
        dataPoint.withNewValue(Some(phase))
    }

    override def restoreState(state: util.List[Integer]): Unit = {
        for (s <- state)
            this.currentStep = s
    }

    @throws[Exception]
    override def snapshotState(checkpointId: Long, checkpointTimestamp: Long): util.List[Integer] = Collections.singletonList(currentStep)
}
