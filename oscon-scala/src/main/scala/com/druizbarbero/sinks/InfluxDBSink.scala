package com.druizbarbero.sink

import com.druizbarbero.data.{DataPoint, KeyedDataPoint}
import com.paulgoldbaum.influxdbclient.Parameter.{Consistency, Precision}
import com.paulgoldbaum.influxdbclient.{Database, InfluxDB, Point, WriteException}
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.{RichSinkFunction, SinkFunction}
import org.slf4j.{Logger, LoggerFactory}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * @author diego@intellisense.io
  */
class InfluxDBSink (val measurement: String) extends RichSinkFunction[DataPoint[_]] {
    val logger: Logger = LoggerFactory.getLogger(getClass)

    @transient var influxDB: InfluxDB = _
    @transient var database: Database = _

    private val fieldName = "value"

    override def open(parameters: Configuration): Unit = {
        super.open(parameters)
        influxDB = InfluxDB.connect("localhost", 8086)
        database = influxDB.selectDatabase("sensors_demo")
    }

    override def invoke(in: DataPoint[_], context: SinkFunction.Context[_]): Unit = {
        val point = Point(measurement, context.timestamp())
                .addField(fieldName, in.value.asInstanceOf[Double])
        if (in.isInstanceOf[KeyedDataPoint[_]])
            point.addTag("key", in.asInstanceOf[KeyedDataPoint[_]].key)
        val write = database.write(point, precision = Precision.MILLISECONDS, consistency = Consistency.ALL, retentionPolicy = "autogen")
        write.recover{case e: WriteException => logger.error(e.getMessage)}
    }

    override def close(): Unit = {
        influxDB.close()
        super.close()
    }
}
