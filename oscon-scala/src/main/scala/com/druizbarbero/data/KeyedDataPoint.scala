package com.druizbarbero.data

case class KeyedDataPoint[T](val key: String, override val timeStampMs: Long, override val value: T)
        extends DataPoint (timeStampMs, value)