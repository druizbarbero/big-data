package com.druizbarbero

import com.druizbarbero.data.KeyedDataPoint
import com.druizbarbero.sink.InfluxDBSink
import com.druizbarbero.source.TimestampedSource
import com.druizbarbero.sources.WatermarksAssigner
import org.apache.flink.api.common.restartstrategy.RestartStrategies
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.slf4j.{Logger, LoggerFactory}

/**
  * @author diego@intellisense.io
  */
object SimpleJob {
    val logger: Logger = LoggerFactory.getLogger(getClass)

    def main(args: Array[String]): Unit = {

        val env = StreamExecutionEnvironment.getExecutionEnvironment()
        env.setParallelism(1)
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(1000, 1000))
        // env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.enableCheckpointing(1000)

        // Initial data - just timestamped messages
        val timestampedSource = env.addSource(new TimestampedSource(100, 1))
        // Simulate some sensor data
        val sensorStream = SensorsGenerator
                .generateSensorData(timestampedSource)
                .assignTimestampsAndWatermarks(WatermarksAssigner)

        sensorStream.print()

        // Write the original stream too out to InfluxDB
        sensorStream.addSink(new InfluxDBSink("raw").asInstanceOf[SinkFunction[KeyedDataPoint[Double]]])

        // Compute a windowed sum over this data and write that to InfluxDB as well
/*
        sensorStream.keyBy("key")
                .timeWindow(Time.seconds(1))
                .sum("value")
                .addSink(new InfluxDBSink("summed").asInstanceOf[SinkFunction[KeyedDataPoint[Double]]])
*/

        // Execute program
        env.execute("Simple Job")
    }
}
