package com.druizbarbero.source

import com.druizbarbero.data.DataPoint
import org.apache.flink.streaming.api.checkpoint.ListCheckpointed
import org.apache.flink.streaming.api.functions.source.{RichSourceFunction, SourceFunction}
import org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.watermark.Watermark
import scala.collection.JavaConversions._
import java.util.{Collections}

/**
  * @author diego@intellisense.io
  */
class TimestampedSource(val periodMs: Int, val slowdownFactor: Int) extends RichSourceFunction[DataPoint[Long]] {

    @volatile private var isRunning = true
    @volatile private var currentTimeMs= 0L;

    override def open(parameters: Configuration): Unit = {
        super.open(parameters)
        val now = System.currentTimeMillis
        if (currentTimeMs == 0) currentTimeMs = now - (now % 1000) // floor to second boundary
    }

    override def run(ctx: SourceContext[DataPoint[Long]]): Unit = {
        while (isRunning) {
            ctx.collectWithTimestamp(new DataPoint[Long](currentTimeMs, Some(0L)), currentTimeMs)
            ctx.emitWatermark(new Watermark(currentTimeMs))
            currentTimeMs += periodMs
        }
        timeSync()
    }

    private def timeSync(): Unit = { // Sync up with real time
        val realTimeDeltaMs = currentTimeMs - System.currentTimeMillis
        var sleepTime = periodMs + realTimeDeltaMs + randomJitter
        if (slowdownFactor != 1) sleepTime = periodMs * slowdownFactor
        if (sleepTime > 0) Thread.sleep(sleepTime)
    }

    private def randomJitter = {
        var sign = -1.0
        if (Math.random > 0.5) sign = 1.0
        (Math.random * periodMs * sign).toLong
    }

    override def cancel(): Unit = isRunning = false

}
