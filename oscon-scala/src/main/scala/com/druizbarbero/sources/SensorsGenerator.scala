package com.druizbarbero

import com.druizbarbero.data.{DataPoint, KeyedDataPoint}
import com.druizbarbero.functions.{AssignKeyFunction, SawtoothFunction, SineWaveFunction, SquareWaveFunction}
import org.apache.flink.streaming.api.datastream.{DataStream, DataStreamSource}

object SensorsGenerator {

    def generateSensorData(timestampedSource: DataStreamSource[DataPoint[Long]]): DataStream[KeyedDataPoint[Double]] = {

        // Transform into a sawtooth pattern
        val sawtoothStream = timestampedSource
                .map(new SawtoothFunction(10))
                .name("sawTooth")

        // From the sawtooth modulate a sine wave
        val sineWaveStream = sawtoothStream
                .map(new SineWaveFunction)
                .name("sineWave")

        // From the sine wave modulate a square wave
        val squareWaveStream = sawtoothStream
                .map(new SquareWaveFunction)
                .name("squareWave")

        // Simulate temp sensor
        val tempStream = sawtoothStream
                .map(new AssignKeyFunction("temp"))
                .name("assignKey(temp)")

        // Simulate pressure sensor
        val pressureStream = sineWaveStream
                .map(new AssignKeyFunction("pressure"))
                .name("assignKey(pressure")

        // Simulate a door sensor
        val doorStream = squareWaveStream
                .map(new AssignKeyFunction("door"))
                .name("assignKey(door)")

        // Combine all the streams into one
        tempStream.union(pressureStream).union(doorStream)
    }
}
