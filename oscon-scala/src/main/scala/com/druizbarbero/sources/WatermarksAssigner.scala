package com.druizbarbero.sources

import com.druizbarbero.data.KeyedDataPoint
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks
import org.apache.flink.streaming.api.watermark.Watermark

object WatermarksAssigner extends AssignerWithPunctuatedWatermarks[KeyedDataPoint[Double]] {

    private[sources] val period = 1000L
    override def checkAndGetNextWatermark(lastElement: KeyedDataPoint[Double], extractedTimestamp: Long) = new Watermark(lastElement.timeStampMs - period)
    override def extractTimestamp(element: KeyedDataPoint[Double], previousElementTimestamp: Long): Long = element.timeStampMs
}
