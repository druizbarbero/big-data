package com.dataartisans.sources;

import com.dataartisans.data.DataPoint;
import com.dataartisans.data.KeyedDataPoint;
import com.dataartisans.functions.AssignKeyFunction;
import com.dataartisans.functions.SawtoothFunction;
import com.dataartisans.functions.SineWaveFunction;
import com.dataartisans.functions.SquareWaveFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;

/**
 * @author diego@intellisense.io
 */
public class SensorsGenerator {

    public DataStream<KeyedDataPoint<Double>> generateSensorData(DataStreamSource<DataPoint<Long>> timestampedSource) {

        // Transform into a sawtooth pattern
        DataStream sawtoothStream = timestampedSource
                .map(new SawtoothFunction(10))
                .name("sawTooth");

        // From the sawtooth modulate a sine wave
        DataStream sineWaveStream = sawtoothStream
                .map(new SineWaveFunction())
                .name("sineWave");

        // From the sine wave modulate a square wave
        DataStream squareWaveStream = sawtoothStream
                .map(new SquareWaveFunction())
                .name("squareWave");

        // Simulate temp sensor
        DataStream tempStream = sawtoothStream
                .map(new AssignKeyFunction("temp"))
                .name("assignKey(temp)");

        // Simulate pressure sensor
        DataStream pressureStream = sineWaveStream
                .map(new AssignKeyFunction("pressure"))
                .name("assignKey(pressure");

        // Simulate a door sensor
        DataStream doorStream = squareWaveStream
                .map(new AssignKeyFunction("door"))
                .name("assignKey(door)");

        // Combine all the streams into one
        return tempStream.union(pressureStream).union(doorStream);
    }
}
