package com.dataartisans.sources;

import com.dataartisans.data.KeyedDataPoint;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;

public class WatermarksAssigner implements AssignerWithPunctuatedWatermarks<KeyedDataPoint<Double>> {

    final long period = 1000L;

    @Override
    public Watermark checkAndGetNextWatermark(KeyedDataPoint<Double> lastElement, long extractedTimestamp) {
        return new Watermark(lastElement.timeStampMs - period);
    }

    @Override
    public long extractTimestamp(KeyedDataPoint<Double> element, long previousElementTimestamp) {
        return element.timeStampMs;
    }
}
