package com.dataartisans.jobs;

import com.dataartisans.data.DataPointSerializationSchema;
import com.dataartisans.sinks.InfluxDBSink;
import com.dataartisans.sources.WatermarksAssigner;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import java.util.Properties;

public class KafkaSensorsConsumerJob {

    public static void main(String[] args) throws Exception {

        // Set up the execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setMaxParallelism(8);
        env.disableOperatorChaining();
        env.getConfig().setLatencyTrackingInterval(1000);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(1000, 1000));
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.enableCheckpointing(1000);

        // Kafka consumer config
        Properties kafkaProperties = new Properties();
        kafkaProperties.setProperty("bootstrap.servers", "localhost:9092");
        kafkaProperties.setProperty("group.id", "sensors-group");

        // Create Kafka Consumer
        FlinkKafkaConsumer09 kafkaConsumer = new FlinkKafkaConsumer09("sensors-demo", new DataPointSerializationSchema(), kafkaProperties);
        // Add it as a source
        SingleOutputStreamOperator sensorStream = env.addSource(kafkaConsumer).assignTimestampsAndWatermarks(new WatermarksAssigner());

        // Write the original stream too out to InfluxDB
        sensorStream.addSink(new InfluxDBSink("raw"));

        // Compute a windowed sum over this data and write that to InfluxDB as well
        sensorStream
                .keyBy("key")
                .timeWindow(Time.seconds(1))
                .sum("value")
                .addSink(new InfluxDBSink<>("summed"));

        // Execute program
        env.execute("Sensors Data Consumer");
    }
}
