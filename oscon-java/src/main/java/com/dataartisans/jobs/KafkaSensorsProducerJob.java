package com.dataartisans.jobs;

import com.dataartisans.data.DataPointSerializationSchema;
import com.dataartisans.sources.SensorsGenerator;
import com.dataartisans.sources.TimestampedSource;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer09;

public class KafkaSensorsProducerJob {

    static final SensorsGenerator generator = new SensorsGenerator();

    public static void main(String[] args) throws Exception {

        // Set up the execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        // Initial data - just timestamped messages
        DataStreamSource timestampedSource = env.addSource(new TimestampedSource(100, 1));
        // Simulate some sensor data
        DataStream sensorsStream = generator.generateSensorData(timestampedSource);

        // Write it to Kafka
        sensorsStream.addSink(new FlinkKafkaProducer09("localhost:9092", "sensors-demo", new DataPointSerializationSchema()));

        // Execute program
        env.execute("Sensors Data Simulator");
    }
}
