package com.dataartisans.jobs;

import com.dataartisans.sinks.InfluxDBSink;
import com.dataartisans.sources.SensorsGenerator;
import com.dataartisans.sources.TimestampedSource;
import com.dataartisans.sources.WatermarksAssigner;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;

public class SimpleJob {

    private static final SensorsGenerator generator = new SensorsGenerator();

    public static void main(String[] args) throws Exception {

        // set up the execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(1000, 1000));
        //env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.enableCheckpointing(1000);

        // Initial data - just timestamped messages
        DataStreamSource timestampedSource = env.addSource(new TimestampedSource(100, 1));
        // Simulate some sensor data
        DataStream sensorStream = generator.generateSensorData(timestampedSource).assignTimestampsAndWatermarks(new WatermarksAssigner());
        sensorStream.print();

        // Write the original stream too out to InfluxDB
        sensorStream.addSink(new InfluxDBSink<>("raw"));

        // Compute a windowed sum over this data and write that to InfluxDB as well
        sensorStream.keyBy("key")
                .timeWindow(Time.seconds(1))
                .sum("value")
                .addSink(new InfluxDBSink<>("summed"));

        // Execute program
        env.execute("Simple Job");
    }
}