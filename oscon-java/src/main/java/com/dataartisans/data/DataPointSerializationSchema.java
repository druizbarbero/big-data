package com.dataartisans.data;

import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.util.serialization.DeserializationSchema;
import org.apache.flink.streaming.util.serialization.SerializationSchema;

@SuppressWarnings("deprecation")
public class DataPointSerializationSchema implements SerializationSchema<KeyedDataPoint<Double>>, DeserializationSchema<KeyedDataPoint<Double>> {

    @Override
    public byte[] serialize(KeyedDataPoint<Double> dataPoint) {
        String s =  dataPoint.timeStampMs + "," + dataPoint.key + "," + dataPoint.value;
        return s.getBytes();
    }

    @Override
    public KeyedDataPoint<Double> deserialize(byte[] bytes) {
        String[] parts = new String(bytes).split(",");
        long timestampMs = Long.valueOf(parts[0]);
        String key = parts[1];
        double value = Double.valueOf(parts[2]);
        return new KeyedDataPoint(key, timestampMs, value);
    }

    @Override
    public boolean isEndOfStream(KeyedDataPoint<Double> doubleKeyedDataPoint) {
        return false;
    }

    @Override
    public TypeInformation<KeyedDataPoint<Double>> getProducedType() {
        return TypeInformation.of(new TypeHint<KeyedDataPoint<Double>>(){});
    }
}
