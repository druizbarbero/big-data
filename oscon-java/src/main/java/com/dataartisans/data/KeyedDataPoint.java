package com.dataartisans.data;


public class KeyedDataPoint<T> extends DataPoint<T>{

    public String key;

    public KeyedDataPoint() {}

    public KeyedDataPoint(String key, long timeStampMs, T value) {
        super(timeStampMs, value);
        this.key = key;
    }

    public <R> KeyedDataPoint<R> withNewValue(R newValue){
        return new KeyedDataPoint(this.key, this.timeStampMs, newValue);
    }

    @Override
    public String toString() {
        return "KeyedDataPoint{" + timeStampMs + '\'' + ", key=" + key + ", value=" + value + '}';
    }
}
