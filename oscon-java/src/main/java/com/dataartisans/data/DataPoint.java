package com.dataartisans.data;

public class DataPoint<T> {

    public long timeStampMs;
    public T value;

    public DataPoint() {}

    public DataPoint(long timeStampMs, T value) {
        this.timeStampMs = timeStampMs;
        this.value = value;
    }

    public <R> DataPoint<R> withNewValue(R newValue){
        return new DataPoint(this.timeStampMs, newValue);
    }

    public KeyedDataPoint withKey(String key){
        return new KeyedDataPoint(key, this.timeStampMs, this.value);
    }
}
