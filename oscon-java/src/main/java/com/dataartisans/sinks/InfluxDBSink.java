package com.dataartisans.sinks;

import com.dataartisans.data.DataPoint;
import com.dataartisans.data.KeyedDataPoint;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import java.util.concurrent.TimeUnit;

public class InfluxDBSink<T extends DataPoint<? extends Number>> extends RichSinkFunction<T> {

    private transient InfluxDB client;
    private String measurement;
    private static String dataBaseName = "sensors_demo";
    private static String fieldName = "value";

    public InfluxDBSink(String measurement){
        this.measurement = measurement;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        client = InfluxDBFactory.connect("http://localhost:8086", "admin", "admin");
        client.createDatabase(dataBaseName);
        client.enableBatch(2000, 100, TimeUnit.MILLISECONDS);
    }

    @Override
    public void invoke(T dataPoint, Context context) throws Exception {
        Point.Builder builder = Point.measurement(measurement)
                .time(dataPoint.timeStampMs, TimeUnit.MILLISECONDS)
                .addField(fieldName, dataPoint.value);
        if(dataPoint instanceof KeyedDataPoint) {
            builder.tag("key", ((KeyedDataPoint) dataPoint).key);
        }
        client.write(dataBaseName, "autogen", builder.build());
    }

    @Override
    public void close() throws Exception {
        super.close();
    }
}
