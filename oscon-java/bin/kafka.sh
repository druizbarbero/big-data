#!/bin/bash

# Start Kafka server
sudo service kafka start

# Create topic
~/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic testing

# Consume from topic
~/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic testing --from-beginning

# Produce to topic
echo "Hello 1" | ~/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic testing > /dev/null