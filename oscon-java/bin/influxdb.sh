#!/bin/bash

# Start Influxdb server
sudo service influxdb start

# Influx client basic query
influx -precision rfc3339
drop database sensors_demo
select * from summed order by time desc limit 20